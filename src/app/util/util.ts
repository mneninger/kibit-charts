import * as moment from 'moment';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

const arrayOfLength = (n: number) => (new Array(n)).fill(null);
const sensorRanges = {
    temperature: { rangeMin: -10, rangeMax: 30 },
    humidity: { rangeMin: 0, rangeMax: 100 },
    light: { rangeMin: 0, rangeMax: 100 }
};
export const generateSensorData = (dateStart: Date, dateEnd: Date, sensor: string): number[][] => {
    const [kind] = sensor.split(' ');
    const { rangeMin, rangeMax } = sensorRanges[kind];
    const days = moment(dateEnd).diff(moment(dateStart), 'days');
    const rangeLength = rangeMax - rangeMin;
    return arrayOfLength(days).map((_, index: number) => {
        const date = moment(dateStart).add(index, 'days').toDate().getTime();
        const value = Math.round(rangeMin + Math.random() * rangeLength);
        return [date, value];
    })
};

export const ngbDateToDate = (date: NgbDate): Date => {
    return new Date(date.year, date.month - 1, date.day);
}
export const dateToNgbDate = (date: Date): NgbDate => {
    return new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate())
}

export const SENSORS = [
    'temperature 1',
    'temperature 2',
    'temperature 3',
    'temperature 4',
    'light 1',
    'light 2',
    'light 3',
    'light 4',
    'humidity 1',
    'humidity 2',
    'humidity 3',
    'humidity 4',
]

export const DEFAULT_DATERANGE = { from: moment().subtract(10, 'days').toDate() , to: new Date() }