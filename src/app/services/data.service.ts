import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  dateArray: Array<number> = [];
  tempArray: Array<Array<number>> = [];

  constructor() { }

  public getDateRangeGenerator(min: number, max: number){
    return 
  }


  private generateDateRange() {
    const startDate = new Date('2019-07-10');
    const endDate = new Date('2019-08-10');
    let currentDate = startDate;
    while (currentDate <= endDate) {
      this.dateArray.push(currentDate.getTime());
      currentDate.setDate(currentDate.getDate() + 1)
    }
    return this.dateArray;
  }

  public generateTemperatureData() {
    this.dateArray = this.generateDateRange();
    if (!this.dateArray.length) return;
    this.dateArray.forEach(element => {
      let temperature = Math.floor(Math.random() * 30) + 1; // this will get a number between 1 and 30;
      temperature *= Math.floor(Math.random() * 2) == 1 ? 1 : -1; // this will add minus sign in 50% of cases
      this.tempArray.push([element, temperature])
    });
    return this.tempArray;
  }
}
