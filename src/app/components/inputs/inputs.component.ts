import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import { SENSORS } from 'src/app/util/util';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent implements OnInit {
  @Output() addNewChart: EventEmitter<any> = new EventEmitter();
  @Input() disabled;
  selectedChartType: string | null = null;
  selectedColor: string | null = null;
  selectedSensors: { name: string, color: string }[] = [];
  chartType = ['line', 'bar']
  colors = ['red', 'green', 'blue', 'orange', 'yellow'];
  sensors = SENSORS;

  constructor() { }

  ngOnInit() {
  }

  selectChartType(value) {
    this.selectedChartType = value;
  }
  selectColor(value) {
    this.selectedColor = value;
  }
  selectSensor(value) {
    if (this.isSelected(value)) {
      this.selectedSensors = this.selectedSensors.filter(({ name }) => name !== value)
    } else {
      this.selectedSensors.push({ name: value, color: this.selectedColor });
    }
  }
  isSelected(value) {
    return !!this.selectedSensors.find(({ name }) => name === value);
  }
  onAddNewChart() {
    this.addNewChart.emit({ type: this.selectedChartType, dataSources: this.selectedSensors })
  }

  setBackgroundColor(sensor) {
    const selected = this.selectedSensors.find(({ name }) => name === sensor);
    return {'background-color': selected ? selected.color : 'none'}
  }

  isDisabled(){
    return this.disabled || !this.selectedChartType || this.selectedSensors.length === 0;
  }
}
