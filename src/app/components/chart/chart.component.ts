import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { InputsComponent } from '../inputs/inputs.component';
import { DataService } from 'src/app/services/data.service';
import { generateSensorData } from '../../util/util'

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() data;
  @Input() type: string;
  @Input() color: string;
  chart;
  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    this.loadFirstChart();
  }

  ngOnChanges() {
    this.loadFirstChart();
  }
  loadFirstChart() {
    this.chart = new Chart({
      title: {
        text: null
      },
      credits: {
        enabled: false
      },
      xAxis: {
        type: 'datetime'
      },
      series: this.data.map(({ name, data, color }) => ({
        name,
        color,
        data,
        type: this.type,
      }))
    });
  }

}

