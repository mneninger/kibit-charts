import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ngbDateToDate, dateToNgbDate, DEFAULT_DATERANGE } from 'src/app/util/util';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent {
  @Output() onDateSelected: EventEmitter<any> = new EventEmitter();
  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;


  constructor(calendar: NgbCalendar) {
    this.fromDate = dateToNgbDate(DEFAULT_DATERANGE.from);
    this.toDate = dateToNgbDate(DEFAULT_DATERANGE.to);
  }


  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
      this.onDateSelected.emit({ from: ngbDateToDate(this.fromDate), to: ngbDateToDate(this.toDate) });

    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }
}
