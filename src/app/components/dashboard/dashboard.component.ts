import { Component, OnInit } from '@angular/core';
import { generateSensorData, SENSORS, DEFAULT_DATERANGE } from 'src/app/util/util';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  charts = [
    { type: 'line', color: 'blue', dataSources: [{ name: 'temperature 1', data: generateSensorData(DEFAULT_DATERANGE.from, DEFAULT_DATERANGE.to, 'temperature 1'), }] }
  ]
  sensors = Object.assign({}, ...SENSORS.map((sensor)=>({
    [sensor]: generateSensorData(DEFAULT_DATERANGE.from, DEFAULT_DATERANGE.to, sensor)
  })))

  constructor() { }

  ngOnInit() {
  }
  onNewChartAdd(chart) {
    this.charts.push({
      ...chart,
      dataSources: chart.dataSources.map((dataSource) => ({
        ...dataSource,
        data: this.sensors[dataSource.name]
      }))
    });
  }

  onDateSelected({ from, to }) {
    this.sensors = Object.assign({}, ...SENSORS.map((sensor)=>({
      [sensor]: generateSensorData(from, to, sensor)
    })))
    this.charts = this.charts.map((chart) => ({
      ...chart,
      dataSources: chart.dataSources.map((dataSource) => ({
        ...dataSource,
        data: this.sensors[dataSource.name]
      }))
    }))
  }

  isDisabled(){
    return this.charts.length > 3;
  }
}

