# KibitCharts

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.3.

## Deployed version
Deployed version can be found [here](https://nervous-aryabhata-78c55b.netlify.com/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## How to use 

The Chart type dropdown and at least one sensor selected is required to add new sensor data chart to the dashboard. 
To select multiple sensordata open the dropdown and you can select and unselect them. 
The chart data is added with the currently selected color. You can change colors, between selecting sensors.
The selected sensors are highlighted with their color, or with green-highlight color if you wish to stay with the default. 

The maximum number of charts are four, and there is one displayed by default. 

The daterange picker updates all charts. 

The different sensor data is randomly generated with appropriate value range, which is -10 to 30 for temperature, and for humidity and light it's always between 0 and 100.

